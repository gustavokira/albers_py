#mudar para cor dominante

import numpy as np
import cv2
import json
import sys

def toHexRGB(r,g,b):
    return '#%02x%02x%02x' % (r, g, b)
def toFrameObject(pixels, labels, centroids, count):
    frame = {}
    frame['id'] = count
    frame['colors'] = []
    for i in range(n_colors):
         X = pixels[labels.ravel() == i]
         color = {}
         color['count'] = len(X)
         b, g, r = centroids[i]
         color['center'] = toHexRGB(int(r),int(g),int(b))
         frame['colors'].append(color)
    return frame

input = sys.argv[1]
output = sys.argv[2]
title = sys.argv[3]

video = cv2.VideoCapture(input)
width = int(video.get(3))
height = int(video.get(4))
fps = video.get(cv2.CAP_PROP_FPS)

converted_frames = []
count = 0

frames_interval = 24

while(video.isOpened()):
    ret, frame = video.read()
    hist = dict()

    if ret:
        if count%frames_interval == 0:

            arr = np.float32(frame)
            pixels = arr.reshape((-1, 3))

            n_colors = 5
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 200, .1)
            flags = cv2.KMEANS_RANDOM_CENTERS
            _, labels, centroids = cv2.kmeans(pixels, n_colors, None, criteria, 20, flags)

            f = toFrameObject(pixels, labels, centroids, count)
            converted_frames.append(f)

        if count%1000 == 0:
            print('frame:'+str(count))

        count = count+1

    else:
        break

for f in converted_frames:
    f['colors'].sort(key=lambda x: x['count'], reverse=False)

info = {}
info['max'] = width*height
info['frames'] = converted_frames
info['title'] = title
with open(output, 'w') as fp:
    json.dump(info, fp)

video.release()
cv2.destroyAllWindows()
