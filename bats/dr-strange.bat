ffmpeg -i originals/doctor-strange-720p.mkv -vf scale=160:64 transformed/doctor-strange-160p.mkv
python src/pixel-color-histogram.py transformed/doctor-strange-160p.mkv data/doctor-strange.json "Doctor Strange"
