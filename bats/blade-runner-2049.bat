ffmpeg -i originals/blade-runner-2049-720p.avi -vf scale=160:64 transformed/blade-runner-2049-160p.avi
python src/pixel-color-histogram.py transformed/blade-runner-2049-160p.avi data/blade-runner-2049.json "Blade Runner 2049"
