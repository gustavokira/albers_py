ffmpeg -i originals/columbus-720p.mkv -vf scale=160:64 transformed/columbus-160p.mkv
python src/pixel-color-histogram.py transformed/columbus-160p.mkv data/columbus.json "Columbus"
