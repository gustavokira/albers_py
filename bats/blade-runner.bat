ffmpeg -i originals/blade-runner-720p.mp4 -vf scale=160:64 transformed/blade-runner-160p.mp4
python src/pixel-color-histogram.py transformed/blade-runner-160p.mp4 data/blade-runner.json "Blade Runner"
